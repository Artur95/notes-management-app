package tests;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.App;
import main.utils.AppConstants;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.io.IOException;

import static javafx.scene.input.KeyCode.ENTER;
import static org.junit.Assert.assertEquals;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.hasChildren;
import org.testfx.matcher.control.ButtonMatchers;
import org.testfx.matcher.control.TextInputControlMatchers;
import org.testfx.matcher.control.LabeledMatchers;

/**
 * Created by Artur on 18.10.2018.
 */
public class LoggingWindowTest extends ApplicationTest {
    private Parent root;

    @Override
    public void start(Stage primaryStage) throws IOException {
        root = FXMLLoader.load(getClass().getResource("../main/views/logging.fxml"));
        primaryStage.setTitle("Notes Management Application - Logging Window");
        primaryStage.setScene(new Scene(root, AppConstants.LOGGING_WINDOW_WIDTH, AppConstants.LOGGING_WINDOW_HEIGHT));
        primaryStage.show();
    }


    @Test
    public void displayMessageWhenFieldsNotFilled() throws Exception {
        verifyThat("#usernameField", TextInputControlMatchers.hasText(""));
        verifyThat("#passwordField", TextInputControlMatchers.hasText(""));
        //Thread.sleep(1000);
        clickOn("#btnSign");
        //Thread.sleep(1000);
        clickOn("OK");
        //Thread.sleep(1000);
    }

    @Test
    public void checkIfButtonsAndLabelsHaveCorrectValue() {
        verifyThat("#btnSign", LabeledMatchers.hasText("Sign In"));
        verifyThat("#btnRegister", LabeledMatchers.hasText("Register"));
        verifyThat("#lblUsername", LabeledMatchers.hasText("User Name:"));
        verifyThat("#lblPassword", LabeledMatchers.hasText("Password:"));
    }

}

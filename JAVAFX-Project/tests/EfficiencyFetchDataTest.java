package tests;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.controllers.AdminController;
import main.controllers.NoteController;
import main.models.Note;
import main.models.NoteDao;
import main.models.User;
import main.models.UserDao;
import main.utils.AppConstants;
import main.utils.DButil;
import main.utils.NeedDBConnection;
import org.junit.*;
import org.apache.commons.lang.time.StopWatch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import java.time.Duration;
import java.time.Instant;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Artur on 28.11.2018.
 */
public class EfficiencyFetchDataTest {
    private static final String CHAR_LIST =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    @NeedDBConnection
    @Before
    public void setup() {
        File file=new File(AppConstants.TESTING_DBNAME.substring(12, 19));
        if (file.exists()) {
            if (file.delete()) {System.out.println("Deleted ...");}
        }

        try {
            DButil.createDatabase(AppConstants.TESTING_DBNAME);
            DButil.dbConnect(AppConstants.TESTING_DBNAME);
            DButil.createTables();
            DButil.dbDisconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private User prepareData(int n) throws Exception {
        DButil.dbConnect(AppConstants.TESTING_DBNAME);
        User user=new User(10, GenerateString(6), GenerateString(10), GenerateString(10), GenerateString(10));
        UserDao.insertUser(user);
        for (int i=0; i<n; i++) {
            NoteDao.insertNote(new Note(null, user.getId(), GenerateString(6), GenerateString(100), LocalDate.parse("2017/11/09", AppConstants.dateFormatter).toString()));
        }
        DButil.dbDisconnect();
        return user;
    }

    @Ignore
    @NeedDBConnection
    @Test
    public void fetchUsersTest() throws Exception {

        AdminController controller=new AdminController();
        StopWatch stopwatch=new StopWatch();
        //Instant instant, instant2;
        BufferedWriter buffer=new BufferedWriter(new FileWriter("userstest.txt"));
        buffer.write("Users_Number" + " " + "Execution_Time");
        buffer.newLine();
        System.out.println("Test Efficiency Users in progress ...");
        stopwatch.start();
        stopwatch.stop();
        stopwatch.reset();
        for (int i = 1; i <= 100; i++) {
            DButil.dbConnect(AppConstants.TESTING_DBNAME);
            User user = new User(null, GenerateString(6), GenerateString(10), GenerateString(10), GenerateString(10));
            UserDao.insertUser(user);
            DButil.dbDisconnect();

            stopwatch.start();
            //instant=Instant.now();
            for (int j = 0; j < 1000; j++) {
                controller.fetchData();
            }
            //instant2=Instant.now();
            stopwatch.stop();
            long t = stopwatch.getTime();
            //long t=Duration.between(instant, instant2).getNano()/1000000;
            stopwatch.reset();
            System.out.println(i + " " + t);
            buffer.write(i + " " + t);
            //System.out.println(i + ".: " + t + " ms");
            buffer.newLine();
        }
        buffer.close();
    }

    @SuppressWarnings("Duplicates")
    private static String GenerateString(int n) {
        StringBuilder randStr = new StringBuilder();
        for(int i=0; i<n; i++){
            char ch = CHAR_LIST.charAt(generateRandomNumber());
            if ((i+1)%40==0) randStr.append("\n");
            randStr.append(ch);
        }
        return randStr.toString();
    }

    private static int generateRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }

    @Ignore
    @NeedDBConnection
    @Test
    public void fetchNotesTest() throws Exception {
        NoteController controller=new NoteController();
        StopWatch stopwatch=new StopWatch();
        //Instant instant, instant2;
        DButil.dbConnect(AppConstants.TESTING_DBNAME);
        User user = new User(10, GenerateString(6), GenerateString(10), GenerateString(10), GenerateString(10));
        UserDao.insertUser(user);
        DButil.dbDisconnect();
        stopwatch.start();
        stopwatch.stop();
        stopwatch.reset();
        for (int k=10; k<=100000; k*=100) {
            System.out.println("Test Efficiency Notes " + k + " in progress ...");
            BufferedWriter buffer = new BufferedWriter(new FileWriter("notestest"+k+".txt"));
            buffer.write("Notes_Number_["+k+"]" + " " + "Execution_Time");
            buffer.newLine();

            NoteController.currentUser = user;
            for (int i = 1; i <= 100; i++) {
                DButil.dbConnect(AppConstants.TESTING_DBNAME);
                Note note = new Note(null, user.getId(), GenerateString(6), GenerateString(k), LocalDate.parse("2017/11/09", AppConstants.dateFormatter).toString());
                NoteDao.insertNote(note);
                DButil.dbDisconnect();
                stopwatch.start();
                //instant=Instant.now();
                for (int j = 0; j < 1000; j++) {
                    controller.fetchData();
                }
                //instant2=Instant.now();
                stopwatch.stop();
                long t = stopwatch.getTime();
                stopwatch.reset();
                //long t=Duration.between(instant, instant2).getNano()/1000000;
                buffer.write(i + " " + t);
                System.out.println(i + " " + t);
                buffer.newLine();
            }
            buffer.close();
        }
    }

    @Test
    public void insertDeleteNoteTest() throws Exception {
        StopWatch stopwatch=new StopWatch();
        //Instant instant, instant2;
        BufferedWriter buffer=new BufferedWriter(new FileWriter("note_insert.txt"));
        BufferedWriter buffer2=new BufferedWriter(new FileWriter("note_delete.txt"));
        buffer.write("Notes_Number" + " " + "Execution_Time");
        buffer2.write("Notes_Number" + " " + "Execution Time");
        buffer.newLine();
        buffer2.newLine();
        System.out.println("Test Efficiency Inserting/Deleting notes in progress ...");
        DButil.dbConnect(AppConstants.TESTING_DBNAME);
        User user = new User(10, GenerateString(6), GenerateString(10), GenerateString(10), GenerateString(10));
        UserDao.insertUser(user);
        stopwatch.start();
        stopwatch.stop();
        stopwatch.reset();
        ArrayList<Note> list=new ArrayList<Note>();
        for (int k=10; k<=100000; k*=10) {
            stopwatch.start();
            //instant=Instant.now();
            for (int j = 0; j < 1000; j++) {
                Note note=new Note(0, user.getId(), GenerateString(6), GenerateString(k), LocalDate.parse("2017/11/09", AppConstants.dateFormatter).toString());
                list.add(note);
                NoteDao.insertNote(note);
            }
            //instant2=Instant.now();
            stopwatch.stop();
            long t = stopwatch.getTime();
            stopwatch.reset();
            stopwatch.start();

            for (int j=0; j<1000; j++) {
                NoteDao.deleteNote(list.get(j));
            }
            stopwatch.stop();
            long t2=stopwatch.getTime();
            //long t=Duration.between(instant, instant2).getNano()/1000000;
            stopwatch.reset();
            System.out.println(k + " Insert " + t + " Delete " + t2);
            buffer.write(k + " " + t);
            buffer2.write(k + " " + t2);
            //System.out.println(i + ".: " + t + " ms");
            buffer.newLine();
        }
        DButil.dbDisconnect();
        buffer.close();
        buffer2.close();
    }

    @After
    public void cleanup() {

    }
}

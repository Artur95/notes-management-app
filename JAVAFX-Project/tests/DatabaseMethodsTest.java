package tests;

import junit.framework.TestCase;
import main.models.Note;
import main.models.NoteDao;
import main.models.User;
import main.models.UserDao;
import main.utils.AppConstants;
import main.utils.DButil;
import org.junit.*;

import java.io.File;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Artur on 02.11.2018.
 */
public class DatabaseMethodsTest {

    @BeforeClass
    public static void setup() throws SQLException {
        File file=new File(AppConstants.TESTING_DBNAME.substring(12, 19));
        if (file.exists()) {
            if (file.delete()) {System.out.println("Deleted ...");}
        }
        try {
            DButil.dbConnect(AppConstants.TESTING_DBNAME);
            DButil.createDatabase(AppConstants.TESTING_DBNAME);
            DButil.createTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        fillDatabase();
    }

    private static void fillDatabase() throws SQLException {
        UserDao.insertUser(new User(0, "Jan", "Kowalski", "Kowal", "Barcelona10"));
        UserDao.insertUser(new User(1, "Piotr", "Nowak", "Now88", "Karabach1"));
        UserDao.insertUser(new User(2, "Katarzyna", "Wielka", "KatWiel91", "Petersburg44"));
        UserDao.insertUser(new User(3, "Piotr", "Kowalski", "Pkow", "Tokio23"));
        NoteDao.insertNote(new Note(0, 1, "Tytul", "Zawartosc", LocalDate.parse("2017/11/09", AppConstants.dateFormatter).toString()));
        NoteDao.insertNote(new Note(1, 0, "Tytul not", "notatki", LocalDate.parse("2015/02/21", AppConstants.dateFormatter).toString()));
        NoteDao.insertNote(new Note(2, 2, "Tytul notatki", "lorem ipsum", LocalDate.parse("2016/10/08", AppConstants.dateFormatter).toString()));
        NoteDao.insertNote(new Note(3, 2, "Taki sobie tytuł", "tak", LocalDate.parse("2014/06/07", AppConstants.dateFormatter).toString()));
        NoteDao.insertNote(new Note(4, 0, "Tytulek", "... zawartość", LocalDate.parse("2012/12/12", AppConstants.dateFormatter).toString()));
        NoteDao.insertNote(new Note(5, 1, "T", "notatka", LocalDate.parse("2018/05/11", AppConstants.dateFormatter).toString()));
        NoteDao.insertNote(new Note(6, 1, "Lista", "Lista jakas tam", LocalDate.parse("2018/08/04", AppConstants.dateFormatter).toString()));
        NoteDao.insertNote(new Note(7, 0, "Tekst", "tekst ...//", LocalDate.parse("2017/10/29", AppConstants.dateFormatter).toString()));
    }

    @Test
    public void getAllUsersTest() throws SQLException, ClassNotFoundException {
        List<User> users=UserDao.getAllUsers();
        assertEquals(4, users.size());
        assertEquals("Jan", users.get(0).getName());
        assertEquals("Kowalski", users.get(0).getSurname());
        assertEquals("Kowal", users.get(0).getUsername());

        assertEquals("Katarzyna", users.get(2).getName());
        assertEquals("Wielka", users.get(2).getSurname());
        assertEquals("KatWiel91", users.get(2).getUsername());

    }

    @Ignore
    @Test
    public void insertDeleteUserTest() throws SQLException, ClassNotFoundException {
        assertEquals(4, UserDao.getAllUsers().size());
        User user=new User(4, "Sebastian", "Sosnowski", "Sebsos89", "bieszczady");
        UserDao.insertUser(user);
        assertEquals(5, UserDao.getAllUsers().size());

        User u=UserDao.getUsersByName("Sebastian").get(0);
        assertEquals("Sosnowski", u.getSurname());
        assertEquals("Sebsos89", u.getUsername());
        UserDao.deleteUser(user);
        assertEquals(4, UserDao.getAllUsers().size());

    }

    @Ignore
    @Test
    public void updateUserTest() {

    }


    @Test
    public void getUsersByNameTest() throws SQLException, ClassNotFoundException {
        List<User> users=UserDao.getUsersByName("Piotr");
        for (User user : users) {
            System.out.println(user.getSurname());
        }
        assertEquals(2, users.size());
        assertEquals("Nowak", users.get(0).getSurname());
        assertEquals("Kowalski", users.get(1).getSurname());
    }

    @Test
    public void getUsersBySurnameTest() throws SQLException, ClassNotFoundException {
        List<User> users=UserDao.getUsersBySurname("Kowalski");
        assertEquals(2, users.size());
        assertEquals("Jan", users.get(0).getName());
        assertEquals("Piotr", users.get(1).getName());
    }

    @Test
    public void getUsersByPartNameTest() throws SQLException, ClassNotFoundException {
        List<User> users=UserDao.getUsersByPartName("Kat");
        assertEquals(1, users.size());
        assertEquals("Katarzyna", users.get(0).getName());
        assertEquals("Wielka", users.get(0).getSurname());
        assertEquals("KatWiel91", users.get(0).getUsername());
    }

    @Test
    public void getUsersByPartSurnameTest() throws SQLException, ClassNotFoundException {
        List<User> users=UserDao.getUsersByPartSurname("alsk");
        assertEquals(2, users.size());

        assertEquals("Jan", users.get(0).getName());
        assertEquals("Kowalski", users.get(0).getSurname());
        assertEquals("Kowal", users.get(0).getUsername());

        assertEquals("Piotr", users.get(1).getName());
        assertEquals("Kowalski", users.get(1).getSurname());
        assertEquals("Pkow", users.get(1).getUsername());
    }

    @Test
    public void getUsersByPartUsername() throws SQLException, ClassNotFoundException {
        List<User> users=UserDao.getUsersByPartUsername("at");
        assertEquals(1, users.size());
        assertEquals("Katarzyna", users.get(0).getName());
        assertEquals("Wielka", users.get(0).getSurname());
        assertEquals("KatWiel91", users.get(0).getUsername());
    }


    @AfterClass
    public static void disconnect() throws SQLException {
        DButil.dbDisconnect();
    }

}

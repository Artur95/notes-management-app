package tests;

import junit.framework.TestCase;
import main.utils.AppConstants;
import main.utils.DButil;
import org.junit.*;

import java.io.File;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Artur on 02.11.2018.
 */
public class DButilTest {

    @BeforeClass
    public static void createDatabaseTest() {
        File file=new File(AppConstants.TESTING_DBNAME.substring(12, 19));
        if (file.exists()) {
            if (file.delete()) {System.out.println("Deleted ...");}
        }
        assertEquals(false, IsFileExist(AppConstants.TESTING_DBNAME));
        try {
            DButil.createDatabase(AppConstants.TESTING_DBNAME);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertEquals(true, IsFileExist(AppConstants.TESTING_DBNAME));
    }

    /*
    @Ignore
    @AfterClass
    public static void dropDatabaseTest() {
        assertEquals(true, IsFileExist(AppConstants.TESTING_DBNAME));

        File file=new File(AppConstants.TESTING_DBNAME.substring(12, 19));
        if (file.exists()) System.out.println("Istnieje");
        try {
            DButil.dbDisconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        file.deleteOnExit();

        assertEquals(false, IsFileExist(AppConstants.TESTING_DBNAME));
    }*/

    private static boolean IsFileExist(String filename) {
        File []files=new File("./").listFiles();
        if (files==null) return false;
        for (File file : files) {
            if (filename.equals("jdbc:sqlite:"+file.getName())) {
                return true;
            }
        }
        return false;
    }



    @Test
    public void createTablesTest() throws Exception {
        DatabaseMetaData metadata=DButil.getConnection().getMetaData();
        ResultSet rs=metadata.getTables(null, null, "%", null);
        assertEquals(false, rs.next());

        try {
            DButil.createTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        metadata=DButil.getConnection().getMetaData();
        rs=metadata.getTables(null, null, "%", null);
        rs.next();
        assertEquals("Notes", rs.getString(3));
        rs.next();
        assertEquals("Users", rs.getString(3));
    }

    @Test
    public void dbConnectionTest() {
        assertEquals(null, DButil.getConnection());
        try {
            DButil.dbConnect(AppConstants.TESTING_DBNAME);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertEquals(true, (DButil.getConnection() != null));
        try {
            DButil.dbDisconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertEquals(null, DButil.getConnection());

        try {
            DButil.dbConnect(AppConstants.TESTING_DBNAME);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}

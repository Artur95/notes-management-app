package main.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import main.App;
import main.models.User;
import main.models.UserDao;
import main.utils.AppConstants;
import main.utils.DButil;
import main.utils.NeedDBConnection;
import main.utils.UserRegisterChecker;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by Artur on 25.07.17.
 */
public class RegistrationController implements Initializable {

    static User currentUser;

    @FXML private GridPane gridpane;
    @FXML private TextField name;
    @FXML private TextField surname;
    @FXML private TextField username;
    @FXML private PasswordField password;
    @FXML private PasswordField repeat_password;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (currentUser!=null) {
            name.setText(currentUser.getName());
            surname.setText(currentUser.getSurname());
            username.setText(currentUser.getUsername());
            password.setText(currentUser.getPassword());
            repeat_password.setText(currentUser.getPassword());
        }
    }

    @FXML
    public void back() throws Exception{
        Stage stage=(Stage)gridpane.getScene().getWindow();
        if (!App.logged) {
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/logging.fxml")); //../views/logging.fxml
            stage.setTitle("Notes Management Application - Logging Window");
            stage.setScene(new Scene(root, AppConstants.LOGGING_WINDOW_WIDTH, AppConstants.LOGGING_WINDOW_HEIGHT));
        } else {
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/admin_account_users.fxml")); //../views/admin_account_users.fxml
            stage.setTitle("Notes Management Application - Admin Window");
            stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
        }
        stage.show();
        currentUser=null;
    }

    @NeedDBConnection
    @FXML
    public void registerAction() throws Exception {

        Stage stage=(Stage)gridpane.getScene().getWindow();
        //first check
        UserRegisterChecker checker=new UserRegisterChecker();
        //TODO
        checker.setName(name.getText());
        checker.setSurname(surname.getText());
        checker.setUsername(username.getText());
        checker.setPassword(password.getText(), repeat_password.getText());

        if (username.getText().equals("Root")) {
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Username Error");
            alert.setContentText("Cannot use Reserved 'Root' username.");
            alert.showAndWait();
            return;
        }

        if (!checker.checkCorrectness()) {
            Map<String, String> info=checker.getInfo();
            StringBuilder s= new StringBuilder();
            for (Map.Entry entry : info.entrySet()) {
                if (!entry.getValue().equals(""))
                s.append(String.format("Field '%s': %s\n", entry.getKey(), entry.getValue()));
            }
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("TextFields Info");
            alert.setContentText(s.toString());
            alert.showAndWait();
            return;
        }

        //add to database

        DButil.dbConnect(AppConstants.DBNAME);
        //
        if (UserDao.getUsersByUsername(username.getText()).size()>0) {
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Username Error");
            alert.setContentText("Username '"+ username.getText() +"' already exist.");
            alert.showAndWait();
            DButil.dbDisconnect();
            return;
        }
        //
        if (currentUser==null) {
            User user=new User(0, name.getText(), surname.getText(), username.getText(), password.getText());
            UserDao.insertUser(user);

        } else {
            currentUser.setName(name.getText());
            currentUser.setSurname(surname.getText());
            currentUser.setUsername(username.getText());
            currentUser.setPassword(password.getText());
            UserDao.updateUser(currentUser);
        }
        DButil.dbDisconnect();

        if (!App.logged) {
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registration success");
            alert.setContentText("You have registered successfully.");
            alert.showAndWait();
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/logging.fxml"));//../views/logging.fxml
            stage.setTitle("Notes Management Application - Logging Window");
            stage.setScene(new Scene(root, AppConstants.LOGGING_WINDOW_WIDTH, AppConstants.LOGGING_WINDOW_HEIGHT));
        } else {
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/admin_account_users.fxml"));//../views/admin_account_users.fxml
            stage.setTitle("Notes Management Application - Admin Window");
            stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
        }
        currentUser=null;
    }

}

package main.controllers;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import main.utils.FindingOption;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Artur on 06.08.2018.
 */
public class FindNotesController implements Initializable {

    @FXML public RadioButton rbPhrase;
    @FXML public RadioButton rbPDate;
    @FXML public RadioButton rbPOT;
    @FXML public HBox hBoxPhrase;
    @FXML public HBox hBoxPeriod;
    @FXML public GridPane gridpane;
    @FXML public DatePicker dpickerFrom;
    @FXML public DatePicker dpickerTo;
    @FXML private TextField tfield;
    @FXML private ComboBox<String> comboBox;
    @FXML private DatePicker dpicker;

    private final String optionTitle="By Title";
    private final String optionContent="By Content";

    @SuppressWarnings("Convert2Lambda")
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBox.setItems(FXCollections.observableArrayList(optionTitle, optionContent));
        comboBox.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println( ((ComboBox)event.getSource()).getSelectionModel().getSelectedItem() );
            }
        });
    }

    @SuppressWarnings("ObjectEqualsNull")
    @FXML
    public void startFinding() {
        FindingOption option=new FindingOption();
        if (rbPhrase.isSelected()) {

            if (comboBox.getSelectionModel().getSelectedItem().equals(optionTitle)) {
                option.setPhraseTitle(tfield.getText());
            }
            else if (comboBox.getSelectionModel().getSelectedItem().equals(optionContent)) {
                option.setPhraseData(tfield.getText());
            } else {
                System.out.println("Error selecting ...");
                return;
            }
        }

        if (rbPDate.isSelected()) {
            if (dpicker.getValue()==null) {
                System.out.println("First choose date in correct format");
                return;
            } else {
                option.setDate(dpicker.getValue());
            }
        }

        if (rbPOT.isSelected()) {
            if (dpickerFrom.getValue()==null || dpickerTo.getValue()==null) {
                System.out.println("First choose dates in correct format");
                return;
            } else {

                System.out.println(dpickerFrom.getValue() + "#######@" + dpickerTo.getValue());
                if(dpickerFrom.getValue().compareTo(dpickerTo.getValue())>0) {
                    System.out.println("First date must be before second ...");
                    return;
                }
                option.setDateFrom(dpickerFrom.getValue());
                option.setDateTo(dpickerTo.getValue());
            }
        }

        NoteController.foption=option;

        ( (Stage) gridpane.getScene().getWindow() ).close();
    }

    @FXML
    public void cancel() {
        ( (Stage) gridpane.getScene().getWindow() ).close();
    }

    @FXML
    public void change(ActionEvent event) {
        if (event.getSource().equals(rbPhrase)) {
            hBoxPhrase.setDisable(false);
            dpicker.setDisable(true);
            hBoxPeriod.setDisable(true);
        }
        if (event.getSource().equals(rbPDate)) {
            hBoxPhrase.setDisable(true);
            dpicker.setDisable(false);
            hBoxPeriod.setDisable(true);
        }
        if (event.getSource().equals(rbPOT)) {
            hBoxPhrase.setDisable(true);
            dpicker.setDisable(true);
            hBoxPeriod.setDisable(false);
        }
    }

}

package main.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import main.App;
import main.models.Note;
import main.models.NoteDao;
import main.models.User;
import main.utils.*;
import main.utils.comparators.DateComparator;
import main.utils.comparators.SizeComparator;
import main.utils.comparators.TitleComparator;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * Created by Artur on 07.08.17.
 */
public class NoteController implements Initializable {

    static AccountType accountType;
    public static User currentUser;
    private Note currentNote;
    private Integer currentIndex;
    private ObservableList<Note> list;
    private static String sortMode;

    @FXML private GridPane gridpane;
    @FXML private BorderPane ContentView;
    @FXML private VBox ControlPanel;
    private Button PreviousNote;
    private Button NextNote;
    private HBox hBoxBottom;
    private VBox vBoxBottom;

    private static List<String> sortOptions;
    private final static String sortOptionDasc="Sort By Date Ascending";
    private final static String sortOptionDdesc="Sort By Date Descending";
    private final static String sortOptionTasc="Sort By Title Ascending";
    private final static String sortOptionTdesc="Sort By Title Descending";
    private final static String sortOptionSasc="Sort By Notes Size Ascending";
    private final static String sortOptionSdesc="Sort By Notes Size Descending";

    static FindingOption foption=null;

    static {
        sortOptions= new ArrayList<>();
        sortOptions.add(sortOptionDasc);
        sortOptions.add(sortOptionDdesc);
        sortOptions.add(sortOptionTasc);
        sortOptions.add(sortOptionTdesc);
        sortOptions.add(sortOptionSasc);
        sortOptions.add(sortOptionSdesc);
        sortMode=null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        createConditionalBackLogoutButtons();

        currentIndex=0;

        fillPane();

    }

    private void createConditionalBackLogoutButtons() {
        Button btnBackLogout=new Button();
        btnBackLogout.setId("MenuBtn");
        sortMode=null;
        foption=null;
        if (accountType==AccountType.ADMIN_ACCOUNT) {
            btnBackLogout.setText("Back");
            btnBackLogout.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    Stage stage=(Stage) gridpane.getScene().getWindow();
                    try {
                        Parent root = FXMLLoader.load(getClass().getResource("/main/views/admin_account_users.fxml"));//../views/admin_account_users.fxml
                        stage.setTitle("Notes Management Application - Admin Window");
                        stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
                        stage.show();
                    } catch(Exception ex) {
                        System.out.println(ex.getMessage());
                    }

                }
            });
        }
        if (accountType==AccountType.USER_ACCOUNT) {
            btnBackLogout.setText("Logout");
            btnBackLogout.setOnAction(new EventHandler<ActionEvent>() {
                @SuppressWarnings("Duplicates")
                @Override
                public void handle(ActionEvent e) {
                    try {
                        App.logged=false;
                        Stage stage=(Stage)gridpane.getScene().getWindow();
                        Parent root = FXMLLoader.load(getClass().getResource("/main/views/logging.fxml"));//../views/logging.fxml
                        stage.setTitle("Notes Management Application - Logging Window");
                        stage.setScene(new Scene(root, AppConstants.LOGGING_WINDOW_WIDTH, AppConstants.LOGGING_WINDOW_HEIGHT));
                        stage.show();
                    } catch(Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            });
        }
        ControlPanel.getChildren().add(btnBackLogout);

    }

    @SuppressWarnings("Convert2Lambda")
    private void createFilteringPart() {
        if (foption!=null) {
            HBox filterBox=new HBox();
            filterBox.setId("filterBox");


            String tlabel=null;
            if (foption.getPhraseTitle()!=null) {
                tlabel=String.format("'%s' [title]", foption.getPhraseTitle());
            }
            if (foption.getPhraseData()!=null) {
                tlabel=String.format("'%s' [data]", foption.getPhraseData());
            }
            if(foption.getDate()!=null) {
                tlabel=String.format("'%s' [particular date]", foption.getDate().toString());
            }
            if (foption.getDateFrom()!=null && foption.getDateTo()!=null) {
                tlabel=String.format("'%s - %s' [period of time]", foption.getDateFrom().toString(), foption.getDateTo());
            }

            Label FilterInfo=new Label("Phrase: ' " + tlabel + " '");
            FilterInfo.setId("filter");
            Button btnDeleteFiltering=new Button("*delete");
            btnDeleteFiltering.setId("btnDelete");
            btnDeleteFiltering.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    foption=null;
                    fillPane();
                }
            });
            filterBox.getChildren().addAll(FilterInfo, btnDeleteFiltering);
            hBoxBottom.getChildren().add(filterBox);
        }
        //*****

        if (sortMode!=null) {
            Button btnDeleteSortMode=new Button("*Delete Sort Mode");
            btnDeleteSortMode.setId("btnDelete");
            btnDeleteSortMode.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    sortMode = null;
                    fillPane();
                }
            });
            hBoxBottom.getChildren().add(btnDeleteSortMode);
        }
        //*****

    }

    @SuppressWarnings("UseBulkOperation")
    @NeedDBConnection
    public ObservableList<Note> fetchData() throws Exception {
        list=null;
        DButil.dbConnect(AppConstants.DBNAME);

        if (foption==null) {
            try {
                list=FXCollections.observableArrayList(NoteDao.getNotesByUser(currentUser.getId()));
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            Set<Note> notesSet= new HashSet<>();
            ObservableList<Note> notesList=null;
            if(foption.getPhraseData()!=null) {

                notesList = NoteDao.getNotesByDataFragment(currentUser.getId(), foption.getPhraseData());
                for (Note note : notesList) {
                    notesSet.add(note);
                    //System.out.println("///"+note);
                }
            }
            if (foption.getPhraseTitle()!=null) {
                notesList = NoteDao.getNotesByTitle(currentUser.getId(), foption.getPhraseTitle()); //NoteDao.getNotesByTitle !!!
                for (Note note : notesList) {

                    notesSet.add(note);
                    //System.out.println("/;/;/;" + note);
                }
            }
            if (foption.getDate()!=null) {
                notesList = NoteDao.getNotesByParticularDate(currentUser.getId(), foption.getDate(), AppConstants.dateFormatter);
                for (Note note : notesList) {
                    notesSet.add(note);
                }
            }
            if (foption.getDateFrom()!=null && foption.getDateTo()!=null) {
                notesList = NoteDao.getNotesByPeriodOfTime(currentUser.getId(), foption.getDateFrom(), foption.getDateTo(), AppConstants.dateFormatter);
                for (Note note : notesList) {
                    //System.out.println("&&&");
                    notesSet.add(note);
                }
            }

            DButil.dbDisconnect();

            if (notesList != null) {
                notesList.clear();
                notesList.addAll(notesSet);
            } else {
                return null;
            }
            list=FXCollections.observableArrayList(notesList);
        }
        return list;
    }

    private void prevNote() {
        NextNote.setDisable(false);
        if (currentIndex!=0) {
            currentIndex--;
            fillPane();
            if (currentIndex==0) PreviousNote.setDisable(true);
            else PreviousNote.setDisable(false);
        }

        //System.out.println(currentIndex.toString() + " ... " + list.size());

    }

    private void nextNote() {
        PreviousNote.setDisable(false);
        if (currentIndex!=list.size()-1) {
            currentIndex++;
            fillPane();
            if (currentIndex==list.size()-1) NextNote.setDisable(true);
            else NextNote.setDisable(false);
        }


        //System.out.println(currentIndex.toString() + " ... " + list.size());
    }

    private void fillPane() { //pobiera dane i wyznacza subwidoki (pusty/niepusty)
        try {
            list=fetchData();
            if (sortMode!=null) {
                if (Objects.equals(sortMode, sortOptionDasc)) list.sort(new DateComparator());
                if (Objects.equals(sortMode, sortOptionDdesc)) list.sort(new DateComparator().reversed());
                if (Objects.equals(sortMode, sortOptionTasc)) list.sort(new TitleComparator());
                if (Objects.equals(sortMode, sortOptionTdesc)) list.sort(new TitleComparator().reversed());
                if (Objects.equals(sortMode, sortOptionSasc)) list.sort(new SizeComparator());
                if (Objects.equals(sortMode, sortOptionSdesc)) list.sort(new SizeComparator().reversed());
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

        ContentView.getChildren().clear();
        if (list.size()==0) {
            createEmptyScenario();
        } else {
            createNonEmptyScenario();
        }
    }

    private void createEmptyScenario() {
        Label info=new Label("0 Records ...");
        ContentView.setCenter(info);
        if (foption!=null) {
            hBoxBottom=new HBox();
            hBoxBottom.setSpacing(30);
            //System.out.println("Filtering ...");
            ContentView.setBottom(hBoxBottom);
            createFilteringPart();
        }
    }

    @SuppressWarnings("Convert2Lambda")
    private void createNonEmptyScenario() {
        currentNote=list.get(currentIndex); //tu wychodzi poza zakres czasami (już chyba nie)

        { //Górna część
            GridPane gpane = new GridPane();
            gpane.setId("NoteHeader");
            Label title = new Label("\""+list.get(currentIndex).getTitle()+"\"");
            title.setId("NoteTitle");
            Label date = new Label(list.get(currentIndex).getDate());
            date.setId("NoteDate");
            gpane.addRow(0, title, date);
            ContentView.setTop(gpane);
        }

        { //Środkowa część
            ScrollPane scrolling = new ScrollPane();
            Label content = new Label(list.get(currentIndex).getData());
            content.setId("NoteData");
            scrolling.setContent(content);
            ContentView.setCenter(scrolling);
        }

        { //Dolna część
            vBoxBottom=new VBox();


            hBoxBottom = new HBox();
            hBoxBottom.setSpacing(30);
            Label notenr = new Label((currentIndex + 1) + " / " + (list.size()));
            notenr.setId("NoteNr");

            hBoxBottom.getChildren().add(notenr);
            PreviousNote = new Button("Previous");
            if (currentIndex == 0) {
                PreviousNote.setDisable(true);
            }
            PreviousNote.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    prevNote();

                }
            });
            NextNote = new Button("Next");
            NextNote.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    nextNote();
                }
            });
            if (list.size() == 1) {
                PreviousNote.setDisable(true);
                NextNote.setDisable(true);
            }
            hBoxBottom.getChildren().add(PreviousNote);
            hBoxBottom.getChildren().add(NextNote);
            vBoxBottom.getChildren().add(hBoxBottom);
            ContentView.setBottom(vBoxBottom);

            createFilteringPart();
        }

        { //Prawa część
            VBox vbox=new VBox();
            vbox.setSpacing(20);
            vbox.setPadding(new Insets(10, 10, 10, 10));
            ScrollPane scrolling=new ScrollPane();
            scrolling.setStyle("-fx-pref-width: 200px");
            for (int i=0; i<list.size(); i++) {
                LabelItem labelitem=new LabelItem("\"" + list.get(i).getTitle() + "\"");
                labelitem.setNote(list.get(i));
                labelitem.setIndex(i);
                labelitem.setCursor(Cursor.HAND);
                labelitem.setOnMouseClicked(new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        LabelItem e=(LabelItem) event.getSource();
                        //System.out.println("Clicked" + e.getNote().getTitle()+", "+ e.getText());
                        currentNote=e.getNote();
                        currentIndex=e.getIndex();
                        fillPane();
                    }
                });
                if (i==currentIndex) {
                    labelitem.setStyle("-fx-font-size: 21px; -fx-text-fill: navy");
                }
                vbox.getChildren().add(labelitem);
            }
            scrolling.setContent(vbox);
            ContentView.setRight(scrolling);
        }


    }



    @SuppressWarnings("OptionalIsPresent")
    @FXML
    public void findNotes() throws Exception {

        /*TextInputDialog dialog=new TextInputDialog();
        dialog.setTitle("Find by phrase");
        dialog.setHeaderText("Find ...");
        dialog.setContentText("Phrase: ");
        Optional<String> result=dialog.showAndWait();
        if (result.isPresent()) {
            //keystring=result.get();
        }

        fillPane();*/

        Stage stage=new Stage();
        Parent root=FXMLLoader.load(getClass().getResource("/main/views/find_notes.fxml")); //../views/find_notes.fxml
        stage.setTitle("Finding Options");
        stage.setScene(new Scene(root, 700, 160));
        stage.initModality(Modality.APPLICATION_MODAL);
        //stage.show();
        stage.showAndWait();
        currentIndex=0;
        fillPane();
    }

    //************************Add, Delete, Edit, Back działają

    @FXML
    public void addNote() throws Exception {
        NoteFormController.currentUser=currentUser;
        Stage stage=(Stage) gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/main/views/add_note_form.fxml")); //../views/add_note_form.fxml
        stage.setTitle("Notes Mangement Application - Create Note");
        stage.setScene(new Scene(root, AppConstants.CREATE_OR_UPDATE_NOTE_WINDOW_WIDTH, AppConstants.CREATE_OR_UPDATE_NOTE_WINDOW_HEIGHT));
        stage.show();
    }

    @NeedDBConnection
    @FXML
    public void deleteNote() throws Exception {
        //System.out.println("nnn: " + currentNote);
        DButil.dbConnect(AppConstants.DBNAME);
        NoteDao.deleteNote(currentNote);
        DButil.dbDisconnect();
        currentIndex=0;
        fillPane();
        Alert alert=new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Delete Info");
        alert.setContentText("Note successfully removed.");
        alert.showAndWait();
        /*Stage stage=(Stage) gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("../views/notes_managing.fxml"));
        stage.setTitle("Notes Management Application - Notes Window ("+( (accountType==AccountType.ADMIN_ACCOUNT) ? "Admin" : currentUser.getUsername())+")");
        stage.setScene(new Scene(root, AppConstants.NOTES_MANAGEMENT_WINDOW_WIDTH, AppConstants.NOTES_MANAGEMENT_WINDOW_HEIGHT));
        stage.show();*/
    }

    @FXML
    public void editNote() throws Exception {
        if (currentNote==null) {
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Selecting");
            alert.setContentText("Error of selecting item in list.");
            alert.showAndWait();
        } else {
            NoteFormController.currentNote=currentNote;
            NoteFormController.currentUser=currentUser;
            Stage stage=(Stage) gridpane.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/add_note_form.fxml"));//../views/add_note_form.fxml
            stage.setTitle("Notes Management Application - Edit Note");
            stage.setScene(new Scene(root, AppConstants.CREATE_OR_UPDATE_NOTE_WINDOW_WIDTH, AppConstants.CREATE_OR_UPDATE_NOTE_WINDOW_HEIGHT));
            stage.show();
        }
    }
    //*******************************


    @SuppressWarnings("OptionalIsPresent")
    @FXML
    public void sortNotes() throws Exception {
        ChoiceDialog<String> choiceDialog= new ChoiceDialog<>(sortOptions.get(0), sortOptions);
        choiceDialog.setTitle("Notes sorting");
        choiceDialog.setHeaderText("Sorting Options");
        choiceDialog.setContentText("Set Option");
        Optional<String> result=choiceDialog.showAndWait();
        if (result.isPresent()) {
            //System.out.println("Chosen Option: " + result.get());
            switch(result.get()) {
                case sortOptionDasc : {
                    sortMode=sortOptionDasc;
                } break;
                case sortOptionDdesc : {
                    sortMode=sortOptionDdesc;
                } break;
                case sortOptionTasc : {
                    sortMode=sortOptionTasc;
                } break;
                case sortOptionTdesc : {
                    sortMode=sortOptionTdesc;
                } break;
                case sortOptionSasc : {
                    sortMode=sortOptionSasc;
                } break;
                case sortOptionSdesc : {
                    sortMode=sortOptionSasc;
                } break;
            }
            currentIndex=0;
            fillPane();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @NeedDBConnection
    @FXML
    public void importNote(ActionEvent actionEvent) throws SQLException, IOException {
        FileChooser fileChooser=new FileChooser();
        fileChooser.setTitle("Select txt file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().add(new ExtensionFilter("txt files", "*.txt"));
        //fileChooser.getExtensionFilters().add(new ExtensionFilter("log files", "*.log"));
        //fileChooser.getExtensionFilters().add(new ExtensionFilter("csv files", "*.csv"));

        List<File> files=fileChooser.showOpenMultipleDialog(gridpane.getScene().getWindow());
        if (files!=null) {
            for(File file : files) {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()));
                String currentLine;
                StringBuilder data = new StringBuilder();
                while ((currentLine = bufferedReader.readLine()) != null) {
                    data.append(currentLine);
                    data.append("\n");
                }
                bufferedReader.readLine();
                bufferedReader.close();
                Note note = new Note();
                note.setUserid(currentUser.getId());
                note.setTitle(file.getName().substring(0, file.getName().length() - 4));
                note.setData(data.toString());
                LocalDate date = Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDate();
                //System.out.println(AppConstants.dateFormatter.format(date));
                note.setDate(AppConstants.dateFormatter.format(date));
                DButil.dbConnect(AppConstants.DBNAME);
                NoteDao.insertNote(note);
                DButil.dbDisconnect();
            }
            fillPane();
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @FXML
    public void exportNote(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser=new FileChooser();
        fileChooser.setTitle("Choose catalog");
        fileChooser.setInitialFileName(currentNote.getTitle() + ".txt");
        fileChooser.setSelectedExtensionFilter(new ExtensionFilter("txt files", "*.txt"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        File file=fileChooser.showSaveDialog(gridpane.getScene().getWindow());
        if (file!=null) {
            BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter(file.getPath()));
            for (String str : currentNote.getData().split("\n")) {
                bufferedWriter.write(str);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        }
    }
}

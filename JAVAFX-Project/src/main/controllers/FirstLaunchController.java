package main.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import main.App;
import main.models.User;
import main.models.UserDao;
import main.utils.AppConstants;
import main.utils.DButil;
import main.utils.NeedDBConnection;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by Artur on 16.11.2018.
 */
public class FirstLaunchController implements Initializable {
    @FXML private PasswordField Password;
    @FXML private PasswordField PasswordR;
    @FXML private GridPane gridpane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @NeedDBConnection //...
    @FXML
    public void Begin(ActionEvent actionEvent) throws IOException, SQLException {
        if (!Password.getText().equals(PasswordR.getText())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Both Fields must be the same.");
            alert.show();
        } else {
            //
            App.dbCreatingProcedure();
            DButil.dbConnect(AppConstants.DBNAME);
            UserDao.insertUser(new User(null, "Root", "Root", "Root", Password.getText()));
            DButil.dbDisconnect();
            //
            try {
                Stage stage = (Stage) gridpane.getScene().getWindow();
                //Parent root = FXMLLoader.load(getClass().getResource("../views/logging.fxml"));
                Parent root = FXMLLoader.load(getClass().getResource("/main/views/logging.fxml"));
                stage.setTitle("Notes Management Application - Logging Window");
                stage.setScene(new Scene(root, AppConstants.LOGGING_WINDOW_WIDTH, AppConstants.LOGGING_WINDOW_HEIGHT));
                stage.show();
            } catch(Exception e) {
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.setContentText(e.getCause()+"\n\n" + e.getMessage());
                alert.show();
            }
        }
    }

    public void Default(ActionEvent actionEvent) {
        Password.setText("Root");
        PasswordR.setText("Root");
    }
}

package main.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.models.Note;
import main.models.NoteDao;
import main.models.User;
import main.utils.AccountType;
import main.utils.AppConstants;
import main.utils.DButil;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Artur on 07.08.17.
 */
public class NoteFormController implements Initializable {

    static User currentUser;
    static Note currentNote;

    @FXML private TextField title;
    @FXML private TextArea noteContent;
    @FXML private GridPane gridpane;
    @FXML private Text Create_Edit_title;
    @FXML private Button Add_Update_button;

    @Override
    public void initialize(URL location, ResourceBundle resource) {
        if (currentNote!=null) {
            title.setText(currentNote.getTitle());
            noteContent.setText(currentNote.getData());
            Create_Edit_title.setText("Edit your note");
            Add_Update_button.setText("Update");
        } else {
            Create_Edit_title.setText("Create your note");
            Add_Update_button.setText("Add");
        }
    }

    @FXML
    public void addNote() throws Exception {

        Note note=new Note();
        note.setUserid(currentUser.getId());
        note.setTitle(title.getText());
        note.setData(noteContent.getText());


        LocalDate localdate=LocalDate.now();
        note.setDate(AppConstants.dateFormatter.format(localdate));
        System.out.println(currentNote);

        DButil.dbConnect(AppConstants.DBNAME);
        if (currentNote==null) {
            NoteDao.insertNote(note);
        } else {
            currentNote.setUserid(note.getUserid());
            currentNote.setTitle(note.getTitle());
            currentNote.setData(note.getData());
            currentNote.setDate(note.getDate());
            System.out.println(currentNote);
            NoteDao.updateNote(currentNote);
        }
        DButil.dbDisconnect();

        Stage stage=(Stage) gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/main/views/notes_managing.fxml")); //../views/notes_managing.fxml
        stage.setTitle("Notes Management Application - Notes Window ("+( (NoteController.accountType== AccountType.ADMIN_ACCOUNT) ? "Admin" : currentUser.getUsername())+")");
        stage.setScene(new Scene(root, AppConstants.NOTES_MANAGEMENT_WINDOW_WIDTH, AppConstants.NOTES_MANAGEMENT_WINDOW_HEIGHT));
        stage.show();
        currentNote=null;
    }

    @FXML
    public void back() throws Exception {
        Stage stage=(Stage) gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/main/views/notes_managing.fxml")); //../views/notes_managing.fxml
        stage.setTitle("Notes Management Application - Notes Window ("+( (NoteController.accountType== AccountType.ADMIN_ACCOUNT) ? "Admin" : currentUser.getUsername())+")");
        stage.setScene(new Scene(root, AppConstants.NOTES_MANAGEMENT_WINDOW_WIDTH, AppConstants.NOTES_MANAGEMENT_WINDOW_HEIGHT));
        stage.show();
        currentNote=null;
    }

}

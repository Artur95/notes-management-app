package main.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import main.App;
import main.models.Note;
import main.models.NoteDao;
import main.models.User;
import main.models.UserDao;
import main.utils.*;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Artur on 25.07.17.
 */
public class AdminController implements Initializable {

    static FindingUserOption fuoption=null;


    @FXML private GridPane gridpane;
    @FXML private Button AddUserBtn;
    @FXML private Button DeleteUserBtn;
    @FXML private Button EditUserBtn;
    @FXML private Button ViewUsersNotesBtn;
    @FXML private Button LogoutBtn;
    @FXML private Button FindBtn;

    @FXML private TableView<User> UsersTable;
    @FXML private TableColumn<User, Integer> columnid;
    @FXML private TableColumn<User, String> columnname;
    @FXML private TableColumn<User, String> columnsurname;
    @FXML private TableColumn<User, String> columnusername;
    @FXML private TableColumn<User, String> columnpassword;

    private VBox vbox;

    //@FXML private TextField FindTextField;
    @FXML private VBox menu;

    private static User lastSelectedUser=null;

    private void setSelection(IndexedCell cell) {
        if (cell.isSelected()) {
            UsersTable.getSelectionModel().clearSelection(cell.getIndex());
        } else {
            UsersTable.getSelectionModel().select(cell.getIndex());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void initialize(URL location, ResourceBundle resources) {


        UsersTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        columnid.setCellValueFactory(new PropertyValueFactory<User, Integer>("Id"));
        columnname.setCellValueFactory(new PropertyValueFactory<User, String>("Name"));
        columnsurname.setCellValueFactory(new PropertyValueFactory<User, String>("Surname"));
        columnusername.setCellValueFactory(new PropertyValueFactory<User, String>("Username"));
        columnpassword.setCellValueFactory(new PropertyValueFactory<User, String>("Password"));

        try {
            UsersTable.setItems(FXCollections.observableArrayList(fetchData()));
        } catch(ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        }
        UsersTable.setOnMouseClicked(rowClickEvent);
        if (lastSelectedUser!=null) {
            Label lblSelected=new Label("Selected: " + lastSelectedUser.getName() + " " +lastSelectedUser.getSurname());
            lblSelected.setId("users_filtering");
            menu.getChildren().add(lblSelected);
        } else {
            Label lblSelected=new Label("Not Selected");
            lblSelected.setId("users_filtering");
            menu.getChildren().add(lblSelected);
        }

    }

    @NeedDBConnection
    public List<User> fetchData() throws ClassNotFoundException, SQLException {
        DButil.dbConnect(AppConstants.DBNAME);
        List<User> users=UserDao.getAllUsers();
        DButil.dbDisconnect();
        return users;
    }

    public EventHandler rowClickEvent=new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            try {
                lastSelectedUser=UsersTable.getSelectionModel().getSelectedItem();
                UsersTable.getSelectionModel().clearSelection();
                System.out.println(lastSelectedUser.getName());


                Stage stage=(Stage) gridpane.getScene().getWindow();
                Parent root = FXMLLoader.load(getClass().getResource("/main/views/admin_account_users.fxml"));//../views/admin_account_users.fxml
                stage.setTitle("Notes Management Application - Admin Window");
                stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
                stage.show();
            } catch(Exception ex) {
                System.out.println("Najpierw zaznacz");
                lastSelectedUser=null;

                /*Stage stage=(Stage) gridpane.getScene().getWindow();
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("../views/admin_account_users.fxml"));
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
                stage.setTitle("Notes Management Application - Admin Window");
                stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
                stage.show();*/
            }
        }
    };

    @FXML
    public void addUser() throws Exception {
        Stage stage=(Stage) gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/main/views/registration.fxml"));//../views/registration.fxml
        stage.setTitle("Notes Management Application - Registration Window (Admin)");
        stage.setScene(new Scene(root, AppConstants.REGISTRATION_WINDOW_WIDTH, AppConstants.REGISTRATION_WINDOW_HEIGHT));
        stage.show();
    }

    @NeedDBConnection
    @FXML
    public void deleteUser() throws Exception {
        DButil.dbConnect(AppConstants.DBNAME);
        UserDao.deleteUser(lastSelectedUser);
        UsersTable.setItems(FXCollections.observableList(UserDao.getAllUsers()));
        DButil.dbDisconnect();
        Alert alert=new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Delete Info");
        alert.setContentText("User successfully removed.");
        alert.showAndWait();
        UsersTable.refresh();
    }

    @FXML
    public void editUser() throws Exception {
        if (lastSelectedUser==null) {
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Selecting");
            alert.setContentText("Error of selecting row in table.");
            alert.showAndWait();
        } else {
            RegistrationController.currentUser=lastSelectedUser;
            Stage stage=(Stage) gridpane.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/registration.fxml"));//../views/registration.fxml
            stage.setTitle("Notes Management Application - Registration Window (Admin)");
            stage.setScene(new Scene(root, AppConstants.REGISTRATION_WINDOW_WIDTH, AppConstants.REGISTRATION_WINDOW_HEIGHT));
            stage.show();
        }
    }

    @FXML
    public void viewUsersNotes() throws Exception {
        if (lastSelectedUser!=null) {
            NoteController.currentUser=lastSelectedUser;
            NoteController.accountType=AccountType.ADMIN_ACCOUNT;
            Stage stage=(Stage) gridpane.getScene().getWindow();


            Parent root = FXMLLoader.load(getClass().getResource("/main/views/notes_managing.fxml"));//../views/notes_managing.fxml

            stage.setTitle("Notes Management Application - Notes Window (Admin)");
            stage.setScene(new Scene(root, AppConstants.NOTES_MANAGEMENT_WINDOW_WIDTH, AppConstants.NOTES_MANAGEMENT_WINDOW_HEIGHT));
            stage.show();
        } else {
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Selecting");
            alert.setContentText("Error of selecting row in table.");
            alert.showAndWait();
        }
    }

    private void createFilteringPart() {
        if (fuoption!=null) {
            vbox=new VBox();
            String tlabel=null;
            if (fuoption.getPhraseName()!=null) {
                tlabel=String.format("Phrase: '%s'[Name]", fuoption.getPhraseName());
            }
            if (fuoption.getPhraseSurname()!=null) {
                tlabel=String.format("Phrase: '%s'[Surname]", fuoption.getPhraseSurname());
            }
            if (fuoption.getNumberOfNotes()!=null) {
                tlabel=String.format("Phrase: '%s'[Number]", fuoption.getNumberOfNotes());
            }
            if (fuoption.getNumberOfNotesFrom()!=null && fuoption.getNumberOfNotesTo()!=null) {
                tlabel=String.format("Phrase: '%s-%s'[Range]", fuoption.getNumberOfNotesFrom(), fuoption.getNumberOfNotesTo());
            }
            Label FilterInfo=new Label(tlabel);
            FilterInfo.setId("filter");
            vbox.getChildren().add(FilterInfo);
            Button btnDelete=new Button("*delete");
            btnDelete.setId("btnDelete");
            btnDelete.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    fuoption=null;
                    UsersTable.refresh();
                    menu.getChildren().remove(btnDelete);
                    Stage stage=(Stage) gridpane.getScene().getWindow();
                    Parent root = null;
                    try {
                        root = FXMLLoader.load(getClass().getResource("/main/views/admin_account_users.fxml"));//../views/admin_account_users.fxml
                        stage.setTitle("Notes Management Application - Admin Window");
                        stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
                        stage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            vbox.getChildren().add(btnDelete);
            menu.getChildren().add(vbox);
        }
    }

    private boolean AtLeastOneNoteExists(User user) {
        ObservableList<Note> list = null;
        try {
            list = FXCollections.observableArrayList(NoteDao.getNotesByUser(user.getId()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list != null && list.size() != 0;
    }

    @SuppressWarnings("UseBulkOperation")
    @NeedDBConnection
    @FXML
    public void findUsers() throws Exception { //nie działa jeszcze
        fuoption=null;
        if (vbox!=null) {
            menu.getChildren().remove(vbox);
        }
        Stage stage=new Stage();
        Parent root=FXMLLoader.load(getClass().getResource("/main/views/find_users.fxml"));//../views/find_users.fxml
        stage.setTitle("Finding Options");
        stage.setScene(new Scene(root, AppConstants.USERS_FINDING_WINDOW_WIDTH, AppConstants.USERS_FINDING_WINDOW_HEIGHT));
        stage.initStyle(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        if (fuoption==null) return;

        DButil.dbConnect(AppConstants.DBNAME);
        Set<User> usersSet= new HashSet<>();
        ObservableList<User> usersList=null;
        if (fuoption.getPhraseName()!=null) {
            usersList=UserDao.getUsersByName(fuoption.getPhraseName());
        }
        if (fuoption.getPhraseSurname()!=null) {
            usersList=UserDao.getUsersBySurname(fuoption.getPhraseSurname());
        }
        if (fuoption.getNumberOfNotes()!=null) {
            ObservableList<User> users=UserDao.getAllUsers();
            usersList=FXCollections.observableArrayList(new ArrayList<User>());
            for (User user : users) {
                if (NoteDao.getAllNotesByUserId(user.getId()).size()==fuoption.getNumberOfNotes()) {
                    usersList.add(user);
                }
            }
        }
        if (fuoption.getNumberOfNotesFrom()!=null && fuoption.getNumberOfNotesTo()!=null) {
            ObservableList<User> users=UserDao.getAllUsers();
            usersList=FXCollections.observableArrayList(new ArrayList<User>());
            for (User user : users) {
                int x=NoteDao.getAllNotesByUserId(user.getId()).size();
                if (x>=fuoption.getNumberOfNotesFrom() && x<=fuoption.getNumberOfNotesTo()) {
                    usersList.add(user);
                }
            }
        }
        UsersTable.setItems(usersList);
        UsersTable.refresh();
        createFilteringPart();
        DButil.dbDisconnect();
    }

    @FXML
    public void back() throws Exception{
        Stage stage=(Stage)gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/main/views/admin_account_users.fxml"));//../views/admin_account_users.fxml
        stage.setTitle("Notes Management Application - Admin Window");
        stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
        stage.show();
    }

    @SuppressWarnings("Duplicates")
    @FXML
    public void logout() throws Exception{
        AdminController.fuoption=null; //
        lastSelectedUser=null;
        App.logged=false;
        Stage stage=(Stage)gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/main/views/logging.fxml"));//../views/logging.fxml
        stage.setTitle("Notes Management Application - Logging Window");
        stage.setScene(new Scene(root, AppConstants.LOGGING_WINDOW_WIDTH, AppConstants.LOGGING_WINDOW_HEIGHT));
        stage.show();
    }

}
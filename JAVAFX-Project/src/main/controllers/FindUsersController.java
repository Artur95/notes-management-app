package main.controllers;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import main.utils.FindingUserOption;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Artur on 19.10.2018.
 */
public class FindUsersController implements Initializable {

    @FXML public GridPane gridpane;
    @FXML public ToggleGroup toggleGroup;
    @FXML public RadioButton rbPhrase;
    @FXML public HBox hBoxPhrase;
    @FXML public TextField tfield;
    @FXML public ComboBox comboBox;
    @FXML public RadioButton rbNNotes;
    @FXML public TextField tfieldnn;
    @FXML public RadioButton rbNNotesRange;
    @FXML public TextField tfieldnnFrom;
    @FXML public TextField tfieldnnTo;
    @FXML public HBox hBoxRange;

    private final String optionName="By Name";
    private final String optionSurname="By Surname";


    @SuppressWarnings("unchecked")
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBox.setItems(FXCollections.observableArrayList(optionName, optionSurname));
        comboBox.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println( ((ComboBox)event.getSource()).getSelectionModel().getSelectedItem() );
            }
        });
    }

    public void change(ActionEvent event) {
        if (event.getSource().equals(rbPhrase)) {
            hBoxPhrase.setDisable(false);
            tfieldnn.setDisable(true);
            hBoxRange.setDisable(true);
        }
        if (event.getSource().equals(rbNNotes)) {
            hBoxPhrase.setDisable(true);
            tfieldnn.setDisable(false);
            hBoxRange.setDisable(true);
        }
        if (event.getSource().equals(rbNNotesRange)) {
            hBoxPhrase.setDisable(true);
            tfieldnn.setDisable(true);
            hBoxRange.setDisable(false);
        }
    }

    public void startFinding(ActionEvent event) {
        FindingUserOption option=new FindingUserOption();
        if (rbPhrase.isSelected()) {

            if (comboBox.getSelectionModel().getSelectedItem().equals(optionName)) {
                option.setPhraseName(tfield.getText());
            }
            else if (comboBox.getSelectionModel().getSelectedItem().equals(optionSurname)) {
                option.setPhraseSurname(tfield.getText());
            } else {
                System.out.println("Error selecting ...");
                return;
            }
        }
        if (rbNNotes.isSelected()) {
            try {
                int x = Integer.parseInt(tfieldnn.getText());
                option.setNumberOfNotes(x);
                System.out.println(x+":&&");
            } catch(NumberFormatException e) {
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: Must be Integer Value");
                alert.setContentText("Error of value");
                alert.showAndWait();
            }
        }

        if (rbNNotesRange.isSelected()) {
            try {
                int x = Integer.parseInt(tfieldnnFrom.getText());
                int y=Integer.parseInt(tfieldnnTo.getText());
                option.setNumberOfNotesFrom(x);
                option.setNumberOfNotesTo(y);
                System.out.println(x+":&&"+y);
            } catch(NumberFormatException e) {
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: Must be Integer Value");
                alert.setContentText("Error of value");
                alert.showAndWait();
            }
        }

        AdminController.fuoption=option;

        ( (Stage) gridpane.getScene().getWindow() ).close();


    }

    public void cancel(ActionEvent event) {
        ( (Stage) gridpane.getScene().getWindow() ).close();
    }
}

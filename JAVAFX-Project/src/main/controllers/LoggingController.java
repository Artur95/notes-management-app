package main.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import main.App;
import main.utils.AccountType;
import main.utils.AppConstants;
import main.utils.Authorization;

/**
 * Created by Artur on 25.07.17.
 */
public class LoggingController {

    @FXML private GridPane gridpane;
    @FXML private TextField username;
    @FXML private PasswordField password;

    @FXML
    public void signInAction() throws Exception {
        Authorization authorization=new Authorization();
        authorization.setUsername(username.getText());
        authorization.setPassword(password.getText());
        AccountType TypeOfAccount=authorization.checkAccountType();

        if (TypeOfAccount==AccountType.ERROR_ACCOUNT) {
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error logging");
            alert.setContentText("Error of logging to an account.");
            alert.showAndWait();
        }
        if (TypeOfAccount==AccountType.ADMIN_ACCOUNT) {
            //admin
            App.logged=true;
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Logging info");
            alert.setContentText("Successfully logged as Admin");
            alert.showAndWait();

            Stage stage=(Stage) gridpane.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/admin_account_users.fxml"));//../views/admin_account_users.fxml
            stage.setTitle("Notes Management Application - Admin Window");
            stage.setScene(new Scene(root, AppConstants.ADMIN_WINDOW_WIDTH, AppConstants.ADMIN_WINDOW_HEIGHT));
            //stage.setX(250);
            //stage.setY(100);
            stage.show();
        }
        if (TypeOfAccount== AccountType.USER_ACCOUNT) {
            //user
            App.logged=true;
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Logging info");
            alert.setContentText("Successfully logged as "+ username.getText());
            alert.showAndWait();

            NoteController.currentUser=authorization.getAuthorizedUser();
            NoteController.accountType=AccountType.USER_ACCOUNT;
            Stage stage=(Stage) gridpane.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("/main/views/notes_managing.fxml")); //../views/notes_managing.fxml
            stage.setTitle("Notes Management Application - Notes Window (" + NoteController.currentUser.getUsername() + ")");
            stage.setScene(new Scene(root, AppConstants.NOTES_MANAGEMENT_WINDOW_WIDTH, AppConstants.NOTES_MANAGEMENT_WINDOW_HEIGHT));
            //stage.setX(250);
            //stage.setY(100);
            stage.show();
        }
    }

    @FXML
    public void registerAction() throws Exception{
        Stage stage=(Stage) gridpane.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/main/views/registration.fxml")); //../views/registration.fxml
        stage.setTitle("Notes Management Application - Registration Window");
        stage.setScene(new Scene(root, AppConstants.REGISTRATION_WINDOW_WIDTH, AppConstants.REGISTRATION_WINDOW_HEIGHT));
        stage.show();
    }
}

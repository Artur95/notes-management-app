package main.models;


/**
 * Created by Artur on 14.07.17.
 */
public class User {
    private Integer id;
    private String name;
    private String surname;
    private String username;
    private String password;

    public User(Integer id, String name, String surname, String username, String password) {
        this.id=id;
        this.name=name;
        this.surname=surname;
        this.username=username;
        this.password=password;
    }

    public User() {

    }

    public Integer getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name=name;
    }

    public String getName() {
        return this.name;
    }

    public void setSurname(String surname) {
        this.surname=surname;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setUsername(String username) {
        this.username=username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password=password;
    }

    public String getPassword() {
        return this.password;
    }

    @Override
    public String toString() {
        return (getId() + ", " + getName() + ", " + getSurname() + ", " + getUsername() + ", " + getPassword());
    }

}

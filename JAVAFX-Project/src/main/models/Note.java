package main.models;

/**
 * Created by Artur on 07.08.17.
 */
public class Note {
    private Integer noteid;
    private Integer userid;
    private String title;
    private String data;
    private String date;

    public Note(Integer noteid, Integer userid, String title, String data, String date) {
        this.noteid=noteid;
        this.userid=userid;
        this.title=title;
        this.data=data;
        this.date=date;
    }

    public Note() {

    }

    @SuppressWarnings("WeakerAccess")
    public Integer getNoteid() {
        return noteid;
    }

    @SuppressWarnings("unused")
    public void setNoteid(Integer noteid) {
        this.noteid = noteid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return (getNoteid() + ", " + getUserid() + ", " + getTitle() + ", " + getData() + ", " + getDate());
    }
}

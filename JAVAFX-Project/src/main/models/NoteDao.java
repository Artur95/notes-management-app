package main.models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.utils.DButil;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artur on 07.08.17.
 */
public class NoteDao {

    @SuppressWarnings("unused")
    public static ObservableList<Note> getAllNotes() throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Notes";
        return FXCollections.observableList(getNotes(sql));
    }

    @SuppressWarnings("WeakerAccess")
    public static ObservableList<Note> getAllNotesByUserId(int userid) throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Notes WHERE userid="+userid+";";
        return FXCollections.observableList(getNotes(sql));
    }

    @SuppressWarnings("WeakerAccess")
    public static ObservableList<Note> getNotes(String sql) throws SQLException, ClassNotFoundException {
        List<Note> notesList= new ArrayList<>();
        try {
            Connection connection= DButil.getConnection();
            Statement stmt=connection.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            while (rs.next()) {
                notesList.add(new Note(rs.getInt("noteid"), rs.getInt("userid"), rs.getString("notetitle"), rs.getString("notedata"), rs.getString("notedate")));
            }
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return FXCollections.observableList(notesList);
    }

    @SuppressWarnings("unused")
    public static ObservableList<Note> getNotesById(int id) throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Notes WHERE noteid="+id+";";
        return FXCollections.observableList(getNotes(sql));
    }

    public static ObservableList<Note> getNotesByUser(int id) throws ClassNotFoundException, SQLException  {
        String sql="SELECT * FROM Notes WHERE userid="+id+";";
        return FXCollections.observableList(getNotes(sql));
    }

    @SuppressWarnings("unused")
    public static ObservableList<Note> getNotesByTitle(String title) throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Notes WHERE notetitle="+title+";";
        return FXCollections.observableList(getNotes(sql));
    }

    public static ObservableList<Note> getNotesByDataFragment(int id, String part) throws ClassNotFoundException, SQLException {
        List<Note> list= new ArrayList<>();
        for (Note note : NoteDao.getAllNotesByUserId(id)) {
            if (note.getData().toLowerCase().contains(part.toLowerCase())) {
                list.add(note);
            }
        }
        return FXCollections.observableList(list);
    }

    public static ObservableList<Note> getNotesByParticularDate(int id, LocalDate date, DateTimeFormatter formatter) throws ClassNotFoundException, SQLException {
        List<Note> list= new ArrayList<>();
        for (Note note : NoteDao.getAllNotesByUserId(id)) {
            if (note.getDate().equals(date.format(formatter))) {
                list.add(note);
            }
        }
        return FXCollections.observableList(list);
    }

    public static ObservableList<Note> getNotesByPeriodOfTime(int id, LocalDate dateFrom, LocalDate dateTo, DateTimeFormatter formatter) throws ClassNotFoundException, SQLException {
        List<Note> list= new ArrayList<>();
        for (Note note : NoteDao.getAllNotesByUserId(id)) {
            LocalDate date=LocalDate.parse(note.getDate(), formatter);
            LocalDate date1=LocalDate.parse(dateFrom.format(formatter), formatter);
            LocalDate date2=LocalDate.parse(dateTo.format(formatter), formatter);
            if (date.compareTo(date1)>=0 && date.compareTo(date2)<=0) {
                list.add(note);
            }
        }
        return FXCollections.observableList(list);
    }

    public static ObservableList<Note> getNotesByTitle(int id, String title) throws ClassNotFoundException, SQLException {
        List<Note> list= new ArrayList<>();
        for (Note note : NoteDao.getAllNotesByUserId(id)) {
            if (note.getTitle().toLowerCase().contains(title.toLowerCase())) {
                list.add(note);
            }

        }
        return FXCollections.observableList(list);
    }


    @SuppressWarnings("SqlDialectInspection")
    public static void insertNote(Note note) throws SQLException {
        String sql="INSERT INTO Notes(userid, notetitle, notedata, notedate) VALUES(?, ?, ?, ?);";
        Connection connection=DButil.getConnection();
        try {
            PreparedStatement pstmt=connection.prepareStatement(sql);
            pstmt.setInt(1, note.getUserid());
            pstmt.setString(2, note.getTitle());
            pstmt.setString(3, note.getData());
            pstmt.setString(4, note.getDate());
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @SuppressWarnings("SqlDialectInspection")
    public static void updateNote(Note note) throws SQLException {
        String sql="UPDATE Notes SET userid=?, notetitle=?, notedata=?, notedate=? WHERE noteid=?;";
        Connection connection=DButil.getConnection();
        try {
            PreparedStatement pstmt=connection.prepareStatement(sql);
            pstmt.setInt(1, note.getUserid());
            pstmt.setString(2, note.getTitle());
            pstmt.setString(3, note.getData());
            pstmt.setString(4, note.getDate());
            pstmt.setInt(5, note.getNoteid());
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @SuppressWarnings("SqlDialectInspection")
    public static void deleteNote(Note note) {
        String sql="DELETE FROM Notes WHERE noteid=?";
        Connection connection=DButil.getConnection();
        try {
            PreparedStatement pstmt=connection.prepareStatement(sql);
            pstmt.setInt(1, note.getNoteid());
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}

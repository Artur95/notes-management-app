package main.models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.utils.DButil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artur on 14.07.17.
 */
public class UserDao {

    public static ObservableList<User> getAllUsers() throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Users";
        return FXCollections.observableList(excludeRoot(getUsers(sql)));
    }

    private static ObservableList<User> getUsers(String sql) throws ClassNotFoundException, SQLException {
        List<User> usersList= new ArrayList<>();
        try {
            Connection connection=DButil.getConnection();
            Statement stmt=connection.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            while (rs.next()) {
                usersList.add(new User(rs.getInt("id"), rs.getString("name"), rs.getString("surname"), rs.getString("username"), rs.getString("password")));
            }
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return FXCollections.observableList(usersList);
    }

    @SuppressWarnings({"unused", "SameParameterValue"})
    public static ObservableList<User> getUsersById(int id) throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Users WHERE id="+id+";";
        return FXCollections.observableList(getUsers(sql));
    }

    @SuppressWarnings("SqlDialectInspection")
    public static ObservableList<User> getUsersByName(String name) throws ClassNotFoundException, SQLException {
        //List<User> usersList= new ArrayList<>();
        String sql="SELECT * FROM Users WHERE name='"+name+"';";
        return FXCollections.observableArrayList(excludeRoot(getUsers(sql)));
        /*String sql="SELECT * FROM Users WHERE name=?";
        PreparedStatement pstmt=DButil.getConnection().prepareStatement(sql);
        pstmt.setString(1, name);
        ResultSet rs=pstmt.executeQuery();
        while (rs.next()) {
            usersList.add(new User(rs.getInt("id"), rs.getString("name"), rs.getString("surname"), rs.getString("username"), rs.getString("password")));
        }
        return FXCollections.observableArrayList(usersList);*/
    }

    public static ObservableList<User> getUsersBySurname(String surname) throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Users WHERE surname='"+surname+"';";
        return FXCollections.observableList(excludeRoot(getUsers(sql)));
    }

    public static ObservableList<User> getUsersByUsername(String username) throws ClassNotFoundException, SQLException {
        String sql="SELECT * FROM Users WHERE username='"+username+"';";
        return FXCollections.observableList(getUsers(sql));
    }

    private static ObservableList<User> excludeRoot(ObservableList<User> list) {
        for(User user : FXCollections.observableArrayList(list)) {
            if (user.getUsername().equals("Root")) list.remove(user);
        }
        return list;
    }

    @SuppressWarnings("unused")
    public static ObservableList<User> getUsersByPartName(String partname) throws ClassNotFoundException, SQLException {
        List<User> list= new ArrayList<>();
        for (User user : UserDao.getAllUsers()) {
            if (user.getName().contains(partname)) {
                list.add(user);
            }
        }
        return FXCollections.observableList(list);
    }

    @SuppressWarnings("unused")
    public static ObservableList<User> getUsersByPartSurname(String partsurname) throws ClassNotFoundException, SQLException {
        List<User> list= new ArrayList<>();
        for (User user : UserDao.getAllUsers()) {
            if (user.getSurname().contains(partsurname)) {
                list.add(user);
            }
        }
        return FXCollections.observableList(list);
    }

    @SuppressWarnings("unused")
    public static ObservableList<User> getUsersByPartUsername(String partusername) throws ClassNotFoundException, SQLException {
        List<User> list= new ArrayList<>();
        for (User user : UserDao.getAllUsers()) {
            if (user.getUsername().contains(partusername)) {
                list.add(user);
            }
        }
        return FXCollections.observableList(list);
    }

    @SuppressWarnings({"Duplicates", "SqlDialectInspection"})
    public static void insertUser(User user) throws SQLException {
        String sql="INSERT INTO Users(name, surname, username, password) VALUES(?, ?, ?, ?);";
        Connection connection=DButil.getConnection();
        try {
            PreparedStatement pstmt=connection.prepareStatement(sql);
            pstmt.setString(1, user.getName());
            pstmt.setString(2, user.getSurname());
            pstmt.setString(3, user.getUsername());
            pstmt.setString(4, user.getPassword());
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @SuppressWarnings({"SqlDialectInspection", "Duplicates"})
    public static void updateUser(User user) {
        String sql="UPDATE Users SET name=?, surname=?, username=?, password=? WHERE id=?";
        Connection connection=DButil.getConnection();
        try {
            PreparedStatement pstmt=connection.prepareStatement(sql);
            pstmt.setString(1, user.getName());
            pstmt.setString(2, user.getSurname());
            pstmt.setString(3, user.getUsername());
            pstmt.setString(4, user.getPassword());
            pstmt.setInt(5, user.getId());
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @SuppressWarnings("SqlDialectInspection")
    public static void deleteUser(User user) {
        String sql="DELETE FROM Users WHERE id=?";
        Connection connection=DButil.getConnection();
        try {
            PreparedStatement pstmt=connection.prepareStatement(sql);
            pstmt.setInt(1, user.getId());
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}

package main.utils;

/**
 * Created by Artur on 19.10.2018.
 */
public class FindingUserOption {
    private String phraseName;
    private String phraseSurname;
    private Integer numberOfNotes;
    private Integer numberOfNotesFrom;
    private Integer numberOfNotesTo;

    public String getPhraseName() {
        return phraseName;
    }

    public void setPhraseName(String phraseName) {
        this.phraseName = phraseName;
    }

    public String getPhraseSurname() {
        return phraseSurname;
    }

    public void setPhraseSurname(String phraseSurname) {
        this.phraseSurname = phraseSurname;
    }

    public Integer getNumberOfNotes() {
        return numberOfNotes;
    }

    public void setNumberOfNotes(Integer numberOfNotes) {
        this.numberOfNotes = numberOfNotes;
    }

    public Integer getNumberOfNotesFrom() {
        return numberOfNotesFrom;
    }

    public void setNumberOfNotesFrom(Integer numberOfNotesFrom) {
        this.numberOfNotesFrom = numberOfNotesFrom;
    }

    public Integer getNumberOfNotesTo() {
        return numberOfNotesTo;
    }

    public void setNumberOfNotesTo(Integer numberOfNotesTo) {
        this.numberOfNotesTo = numberOfNotesTo;
    }
}

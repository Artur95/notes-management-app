package main.utils;

import java.time.format.DateTimeFormatter;

/**
 * Created by Artur on 06.08.2018.
 */
public class AppConstants {
    public static final int LOGGING_WINDOW_WIDTH = 700;
    public static final int LOGGING_WINDOW_HEIGHT = 400;
    public static final int REGISTRATION_WINDOW_WIDTH = 700;
    public static final int REGISTRATION_WINDOW_HEIGHT = 400;
    public static final int ADMIN_WINDOW_WIDTH = 1100;
    public static final int ADMIN_WINDOW_HEIGHT = 700;
    public static final int CREATE_OR_UPDATE_NOTE_WINDOW_WIDTH = 700;
    public static final int CREATE_OR_UPDATE_NOTE_WINDOW_HEIGHT = 500;
    public static final int NOTES_MANAGEMENT_WINDOW_WIDTH = 1200;
    public static final int NOTES_MANAGEMENT_WINDOW_HEIGHT = 800;
    public static final int NOTES_FINDING_WINDOW_WIDTH = 700;
    public static final int NOTES_FINDING_WINDOW_HEIGHT = 160;
    public static final int USERS_FINDING_WINDOW_WIDTH = 700;
    public static final int USERS_FINDING_WINDOW_HEIGHT = 160;
    public static final int FIRST_LAUNCH_WINDOW_WIDTH = 600;
    public static final int FIRST_LAUNCH_WINDOW_HEIGHT = 300;

    public static final String DBNAME = "jdbc:sqlite:Baza.db";
    public static final String TESTING_DBNAME = "jdbc:sqlite:Test.db";

    public static final DateTimeFormatter dateFormatter=DateTimeFormatter.ofPattern("yyyy/MM/dd");
}

package main.utils;

import javafx.collections.ObservableList;
import main.models.User;
import main.models.UserDao;

/**
 * Created by Artur on 25.07.17.
 */
public class Authorization {
    private String username;
    private String password;
    private User authUser;

    public void setUsername(String Username) {
        username=Username;
    }

    public void setPassword(String Password) {
        password=Password;
    }

    @NeedDBConnection
    public AccountType checkAccountType() throws Exception {
        if (username.equals("") || password.equals("")) {
            return AccountType.ERROR_ACCOUNT;
        }
        DButil.dbConnect(AppConstants.DBNAME);


        for (User user : UserDao.getAllUsers()) {
            System.out.println(user.getId() + "^^^^^");
        }
        String password_db=UserDao.getUsersById(1).get(0).getPassword();


        if (username.equals("Root") && password.equals(password_db)) {
            return AccountType.ADMIN_ACCOUNT;
        }

        ObservableList<User> usersList = UserDao.getUsersByUsername(username);
        DButil.dbDisconnect();
        if (usersList.size()==0) {
            return AccountType.ERROR_ACCOUNT;
        }
        System.out.println(username+"...."+password);
        for (User user : usersList) {
            System.out.println(user.getUsername() + ", " + user.getPassword() + "__"+password+"++" + username);
            if (user.getPassword().equals(password)) {
                authUser=user;
                return AccountType.USER_ACCOUNT;
            }
        }
        return AccountType.ERROR_ACCOUNT;
    }

    public User getAuthorizedUser() {
        return this.authUser;
    }
}

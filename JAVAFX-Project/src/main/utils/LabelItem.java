package main.utils;

import javafx.scene.control.Label;
import main.models.Note;

/**
 * Created by Artur on 16.11.2018.
 */
public class LabelItem extends Label {
    private Note note;
    private int index;

    public LabelItem(String str) {
        super(str);
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}

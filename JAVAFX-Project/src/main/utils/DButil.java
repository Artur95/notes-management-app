package main.utils;

import java.io.File;
import java.sql.*;

/**
 * Created by Artur on 25.07.17.
 */
public class DButil {
    private static final String DB_PATH="jdbc:sqlite:Baza.db";
    private static Connection connection=null;
    private static String currentFile=null;

    public static void dbConnect(String path) throws SQLException {
        currentFile=path;
        try {
            //String url = "jdbc:sqlite:Baza.db";
            connection = DriverManager.getConnection(path);
            //System.out.println("Connection to SQLite has been established. ["+path+"]");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @SuppressWarnings("unused")
    public static void dbDisconnect() throws SQLException {

        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        connection=null;

    }

    public static Connection getConnection() {
        return connection;
    }

    @SuppressWarnings("unused")
    public static void createDatabase(String dbName) throws SQLException {
        //String url = "jdbc:sqlite:Baza.db";
        //String url="jdbc:sqlite:" + dbName;
        Connection conn = DriverManager.getConnection(dbName);
        if (conn != null) {
            DatabaseMetaData meta = conn.getMetaData();
            System.out.println("The driver name is " + meta.getDriverName());
            System.out.println("DBname: " + dbName);
            System.out.println("A new database has been created.");
        }
    }

    @SuppressWarnings("SqlDialectInspection")
    public static void createTables() throws SQLException {
        String sqltable1="CREATE TABLE IF NOT EXISTS Users(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(50) NOT NULL, surname VARCHAR(50) NOT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(50) NOT NULL);";
        String sqltable2="CREATE TABLE IF NOT EXISTS Notes(noteid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, userid INTEGER NOT NULL, notetitle VARCHAR(50) NOT NULL, notedata VARCHAR(1000) NOT NULL, notedate VARCHAR(20) NOT NULL, FOREIGN KEY(userid) REFERENCES Users(id) ON DELETE CASCADE ON UPDATE CASCADE);";
        Statement stmt=connection.createStatement();
        stmt.execute(sqltable1);
        stmt.execute(sqltable2);
    }

    public static void dropDatabase(String dbName) throws SQLException {
        /*Connection conn=DriverManager.getConnection(dbName);
        Statement stmt=conn.createStatement();
        stmt.executeUpdate("DROP DATABASE " + dbName.substring(12, 16));
        conn.close();*/

        new File(currentFile).delete();
    }


}
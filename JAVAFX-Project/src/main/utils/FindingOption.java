package main.utils;

import java.time.LocalDate;

/**
 * Created by Artur on 13.08.2018.
 */
public class FindingOption {
    private String phraseData;
    private String phraseTitle;
    private LocalDate date;
    private LocalDate dateFrom;
    private LocalDate dateTo;

    public String getPhraseData() {
        return phraseData;
    }

    public void setPhraseData(String phraseData) {
        this.phraseData = phraseData;
    }

    public String getPhraseTitle() {
        return phraseTitle;
    }

    public void setPhraseTitle(String phraseTitle) {
        this.phraseTitle = phraseTitle;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }
}

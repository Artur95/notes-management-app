package main.utils.comparators;

import main.models.Note;

import java.util.Comparator;

/**
 * Created by Artur on 11.08.2018.
 */
public class SizeComparator implements Comparator<Note> {

    @Override
    public int compare(Note note1, Note note2) {
        return ( (note1.getData().length()>note2.getDate().length()) ? 1 : 0);
    }
}

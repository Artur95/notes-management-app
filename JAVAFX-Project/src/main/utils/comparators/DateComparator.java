package main.utils.comparators;

import main.models.Note;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

/**
 * Created by Artur on 11.08.2018.
 */
public class DateComparator implements Comparator<Note> {

    @Override
    public int compare(Note note1, Note note2) {
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return (LocalDate.parse(note1.getDate(), formatter).compareTo(LocalDate.parse(note2.getDate(), formatter)));
    }
}

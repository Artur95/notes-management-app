package main.utils.comparators;

import main.models.Note;

import java.util.Comparator;

/**
 * Created by Artur on 11.08.2018.
 */
public class TitleComparator implements Comparator<Note> {

    @Override
    public int compare(Note note1, Note note2) {
        return ( (note1.getTitle().toLowerCase().compareTo(note2.getTitle().toLowerCase())>=0) ? 1 : 0);
    }
}

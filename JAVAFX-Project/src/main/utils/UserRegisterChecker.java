package main.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Artur on 26.07.17.
 */
public class UserRegisterChecker {
    private String name;
    private String surname;
    private String username;
    private String password;
    private String repeat_password;

    private Map<String, String> info;

    public UserRegisterChecker() {
        info=new HashMap<String, String>();
        info.put("name", "");
        info.put("surname", "");
        info.put("username", "");
        info.put("password", "");
        info.put("repeat_password", "");
    }

    public void setName(String name) {
        this.name=name;
    }

    public void setSurname(String surname) {
        this.surname=surname;
    }

    public void setUsername(String username) {
        this.username=username;
    }

    public void setPassword(String password, String repeat_password) {
        this.password=password;
        this.repeat_password=repeat_password;
    }

    public boolean checkCorrectness() {

        boolean correct=true;
        if (name.equals("")) {info.put("name", info.get("name")+"Empty Field\n"); correct=false;}
        if (surname.equals("")) {info.put("surname", info.get("surname")+"Empty Field\n"); correct=false;}
        if (username.equals("")) {info.put("username", info.get("username")+"Empty Field\n"); correct=false;}
        if (password.equals("")) {info.put("password", info.get("password")+"Empty Field\n"); correct=false;}
        if (repeat_password.equals("")) {info.put("repeat_password", info.get("repeat_password")+"Empty Field\n"); correct=false;}

        if (!checkAtLeastnCharacters(name, 3)) {info.put("name", info.get("name")+"At Least 3 Characters needed\n"); correct=false;}
        if (!checkAtLeastnCharacters(surname, 3)) {info.put("surname", info.get("surname")+"At Least 3 Characters needed\n"); correct=false;}
        if (!checkAtLeastnCharacters(username, 5)) {info.put("username", info.get("username")+"At Least 5 Characters needed\n"); correct=false;}
        if (!checkAtLeastnCharacters(password, 8)) {info.put("password", info.get("password")+"At Least 8 Characters needed\n"); correct=false;}

        if (!checkAtLeastnLetters(name, 3)) {info.put("name", info.get("name")+"At Least 3 Letters needed\n"); correct=false;}
        if (!checkAtLeastnLetters(surname, 3)) {info.put("name", info.get("surname")+"At Least 3 Letters needed\n"); correct=false;}
        if (!checkAtLeastNumber(password)) {info.put("password", info.get("password")+"At Least 1 number needed\n"); correct=false;}


        //check if empty

        /*if (name.equals("") || surname.equals("") || username.equals("") || password.equals("") || repeat_password.equals("")) {
            return false;
        }*/


        //if (!password.equals(repeat_password)) {return false;}

        System.out.println(info.size());
        if (!password.equals(repeat_password)) {
            info.put("password", info.get("password")+"Password and repeat password must include the same value\n");
            info.put("repeat_password", info.get("repeat_password")+"Password and repeat password must include the same value\n");
            correct=false;
        }



        return correct;
        //return true;
    }

    public boolean checkAtLeastnLetters(String s, int n) {
        int k=0;
        for (Character c : s.toCharArray()) {
            if (Character.isLetter(c)) {
               k++;
            }
        }
        return k >= n;
    }

    public boolean checkAtLeastNumber(String s) {
        for (Character c : s.toCharArray()) {
            if (Character.isDigit(c))  {
                System.out.println(s+"::::" + c);
            }
            return true;
        }
        return false;
    }

    public boolean checkAtLeastnCharacters(String s, int n) {
        int k=0;
        for (Character c : s.toCharArray()) {
            k++;
        }
        return k >= n;
    }

    public Map<String, String> getInfo() {


        return info;
    }

}

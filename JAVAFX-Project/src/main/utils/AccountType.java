package main.utils;

/**
 * Created by Artur on 25.07.17.
 */
public enum AccountType {
    ERROR_ACCOUNT(100),
    ADMIN_ACCOUNT(101),
    USER_ACCOUNT(102);

    private int value;

    private AccountType(int value) {
        this.value=value;
    }
    @SuppressWarnings("unused")
    public int getValue() {
        return value;
    }
}

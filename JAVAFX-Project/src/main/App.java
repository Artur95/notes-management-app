package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import main.utils.AppConstants;
import main.utils.DButil;
import main.utils.NeedDBConnection;

import java.io.File;
import java.sql.SQLException;

public class App extends Application {
    public static boolean logged=false;

    @NeedDBConnection
    @Override
    public void start(Stage primaryStage) throws Exception {

        if (!new File(AppConstants.DBNAME.substring(12, 19)).exists()) {
            System.out.println("Database does not exist ...");
            //redirecting to first_launch
            Parent root = FXMLLoader.load(getClass().getResource("views/first_launch.fxml"));
            primaryStage.setTitle("Notes Management Application - First Launch");
            primaryStage.setScene(new Scene(root, AppConstants.FIRST_LAUNCH_WINDOW_WIDTH, AppConstants.FIRST_LAUNCH_WINDOW_HEIGHT));
            primaryStage.show();

            //
        } else {

            dbCreatingProcedure();
            try {
                Parent root = FXMLLoader.load(getClass().getResource("views/logging.fxml"));
                primaryStage.setTitle("Notes Management Application - Logging Window");
                primaryStage.setScene(new Scene(root, AppConstants.LOGGING_WINDOW_WIDTH, AppConstants.LOGGING_WINDOW_HEIGHT));
                primaryStage.show();
            } catch(Exception e) {
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.setContentText(e.getCause()+"\n\n" + e.getMessage());
                alert.show();

            }
        }
    }

    public static void dbCreatingProcedure() throws SQLException {
        try {
            DButil.createDatabase(AppConstants.DBNAME);
            DButil.dbConnect(AppConstants.DBNAME);
            DButil.createTables();
            DButil.dbDisconnect();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}